[![Tag](https://img.shields.io/gitlab/v/tag/japtain_cack/starmade-server?style=for-the-badge)](https://gitlab.com/japtain_cack/starmade-server/-/tags)
[![Gitlab Pipeline](https://img.shields.io/gitlab/pipeline-status/japtain_cack/starmade-server?branch=master&style=for-the-badge)](https://gitlab.com/japtain_cack/starmade-server/-/pipelines)
[![Issues](https://img.shields.io/gitlab/issues/open/japtain_cack/starmade-server?style=for-the-badge)](https://gitlab.com/japtain_cack/starmade-server/-/issues)
[![License](https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge)](https://creativecommons.org/licenses/by-nd/4.0/)


# StarMade Server Docker Container

## Overview
This repository provides a Docker solution for running a StarMade server with automatic updates and configuration management via Remco. It also includes instructions for deploying the server to a Kubernetes cluster using Kustomize.

## Features
- **Automatic Updates**: Ensures the StarMade Server is up-to-date.
- **Remco Configuration Management**: Simplifies environment-specific configurations.
- **Kubernetes Deployment**: Instructions for deploying the server to a Kubernetes cluster using Kustomize.

## Prerequisites
Before deploying the server, ensure:
- The `worlds` directory is mounted outside the container to preserve data.
- File system permissions are set correctly (e.g., `chown 10000:10000 /mount/path`).
- Environment variables are used to customize the `server.cfg` file.
- You have a Kubernetes cluster up and running.
- You have installed and configured Kustomize.

## Quick Start
Launch the server with customized settings using the following command:

```bash
docker run -d -it --name=starmade1 \
  -v /opt/starmade/world1:/home/starmade/server \
  -p 4242:4242/tcp \
  -e STARMADE_WORLD=world1 \
  -e STARMADE_HOST_NAME_TO_ANNOUNCE_TO_SERVER_LIST=sm.example.com \
  -e STARMADE_SERVER_LIST_NAME=starmade \
  -e STARMADE_SERVER_LIST_DESCRIPTION="Starmade server" \
  -e STARMADE_PROTECT_STARTING_SECTOR=true \
  -e STARMADE_SUPER_ADMIN_PASSWORD_USE=true \
  -e STARMADE_SUPER_ADMIN_PASSWORD=secret \
  -e STARMADE_MINING_BONUS=5 \
  nsnow/starmade-server:latest
```

## Deploying to Kubernetes using Kustomize
To deploy the StarMade server to a Kubernetes cluster using Kustomize, follow these steps:

1. Ensure you have a Kubernetes cluster up and running.
2. Install and configure Kustomize.
4. Create a new directory somewhere, recommeded to keep this in git, then create the following kustomize files.


starmade/statefulset-patch.yaml:
```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: starmade-server
spec:
  template:
    spec:
      containers:
      - name: starmade-server
        env:
          - name: STARMADE_WORLD
            value: "starmade.talos.mimir-tech.org"
          - name: STARMADE_HOST_NAME_TO_ANNOUNCE_TO_SERVER_LIST
            value: "starmade.talos.mimir-tech.org"
          - name: STARMADE_SERVER_LIST_NAME
            value: "starmade.talos.mimir-tech.org"
          - name: STARMADE_SERVER_LIST_DESCRIPTION
            value: "starmade.talos.mimir-tech.org"
          - name: STARMADE_PROTECT_STARTING_SECTOR
            value: "true"
          - name: STARMADE_SUPER_ADMIN_PASSWORD_USE
            value: "true"
          - name: STARMADE_SUPER_ADMIN_PASSWORD
            valueFrom:
              secretKeyRef:
                name: starmade-secrets
                key: adminPassword
        resources:
          requests:
            cpu: 1000m
            memory: 2Gi
            ephemeral-storage: 100Mi
          limits:
            ephemeral-storage: 100Mi
```

starmade/secrets.yaml:
```yaml
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: starmade-secrets
spec:
  refreshInterval: "300s"
  secretStoreRef:
    name: vault-backend
    kind: ClusterSecretStore
  target:
    deletionPolicy: Delete
  dataFrom:
  - extract:
      key: starmade
```

starmade/kustomization.yaml:
```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
namespace: starmade

resources:
  - https://gitlab.com/japtain_cack/starmade-server.git
  - secrets.yaml

patches:
  - path: statefulset-patch.yaml
    target:
      group: apps
      version: v1
      kind: StatefulSet
      name: starmade-server

images:
  - name: starmade-server
    newname: registry.gitlab.com/japtain_cack/starmade-server
    newTag: "1"
```

5. Apply the Kustomize configuration to your Kubernetes cluster:

```bash
kubectl apply -k .
```

This will create a StatefulSet and a Service in your Kubernetes cluster, and your StarMade server will be up and running. If you don't use ExternalSecrets,
just make a normal kubernetes secret. However, don't commit secrets to the repo for security reasons.

## Management Commands
- **Stop and Remove Containers**: `docker kill $(docker ps -qa); docker rm $(docker ps -qa)`
- **View Logs**: `docker logs starmade1`
- **Attach to Console**: `docker attach starmade1` (Detach with `CTRL+p` + `CTRL+q`)
- **Access Bash Console**: `docker exec -it starmade1 bash`

> **Note**: Container names are referenced if the `--name` flag is used in the `docker run` command.

## SELinux Context for Volumes
Set the SELinux context for mounted volumes with:
`chcon -Rt svirt_sandbox_file_t /path/to/volume`

## Configuration
- **UID/GID**: Optionally set with `STARMADE_UID` and `STARMADE_GID`.
- **Environment Variables**: Refer to [server.cfg template](https://gitlab.com/japtain_cack/starmade-server/-/blob/master/remco/templates/server.cfg) for all variables.

## Remco Template Keys
Remco templates use keys that map to environment variables, transforming `/starmade/some-option` to `STARMADE_SOME_OPTION`. The `getv()` function in the template sets default values:

```bash
getv("/starmade/some-option", "default-value")
```

To override a default value, use the `-e` flag:

```bash
docker run -e STARMADE_SOME_OPTION=my-value ...
```

For detailed documentation, visit [Remco's GitHub repository](https://github.com/HeavyHorst/remco).
